<?php

function profile_convert_user_fields_batch_info() {
  $batch = array();

  $fields = profile_field_load_all();
  $batch['operations'][] = array('profile_convert_user_fields_batch_convert_fields', array_keys($fields));
  $batch['operations'][] = array('profile_convert_user_fields_batch_disable');
  $batch['operations'][] = array('profile_convert_user_fields_batch_convert_data', array_keys($fields));
  $batch['operations'][] = array('profile_convert_user_fields_batch_uninstall');
  $batch['file'] = drupal_get_path('module', 'profile_convert_user_fields') . '/profile_convert_user_fields.batch.inc';

  return $batch;
}

function profile_convert_user_fields_batch_disable() {
  // Disable the profile module so that we can update fields with the profile
  // values without conflicts.
  module_disable(array('profile'), FALSE);
}

function profile_convert_user_fields_batch_convert_fields(array $profile_fields, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox'] = array();
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($profile_fields);

    // Stop if there are no profile fields to upgrade.
    if (empty($context['sandbox']['max'])) {
      $context['finished'] = 1;
      return;
    }

    $context['sandbox']['field_names'] = profile_convert_user_fields_map_assoc($profile_fields);
    $context['sandbox']['profile_field_names'] = array_keys($context['sandbox']['fields']);
  }

  $index = $context['sandbox']['progress'];
  $profile_field_name = $context['sandbox']['profile_field_names'][$index];
  $field_name = $context['sandbox']['field_names'][$index];
  profile_convert_user_fields_convert_field($profile_field_name, $field_name);

  $context['sandbox']['progress']++;

  if ($context['sandbox']['progress'] < $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

function profile_convert_user_fields_batch_convert_data(array $profile_fields, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox'] = array();
    $context['sandbox']['progress'] = 0;
    // Ensure only users with profile field values for these specific fields
    // are converted.
    $context['sandbox']['fids'] = db_query("SELECT fid FROM {profile_field} WHERE name IN (:names)", array(':names' => $profile_fields))->fetchCol();
    $context['sandbox']['max'] = db_query("SELECT COUNT(DISTINCT uid) FROM {profile_value} WHERE fid IN (:fids) AND uid > 0", array(':fids' => $context['sandbox']['fids']))->fetchField();

    // Stop if there are no users with profile fields to upgrade.
    if (empty($context['sandbox']['max'])) {
      $context['finished'] = 1;
      return;
    }
  }

  $uids = db_query_range("SELECT DISTINCT uid FROM {profile_value} WHERE fid IN (:fids) uid > 0 ORDER BY uid", 0, 25, array(':fids' => $context['sandbox']['fids']));
  $accounts = user_load_multiple($uids);
  foreach ($accounts as $account) {
    profile_convert_user_fields_convert_user_data($account, $profile_fields);
  }

  $context['progress'] += count($uids);

  if ($context['sandbox']['progress'] < $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

function profile_convert_user_fields_batch_uninstall() {
  // Uninstall the profile module, removing its tables.
  drupal_uninstall_modules(array('profile'), FALSE);
}
